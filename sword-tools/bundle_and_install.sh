#!/bin/bash


## Must modify these:
SWORDTOOLS="$HOME/devel/sword-tools"
CALCOMSOURCES="$HOME/christian/books/John Calvin/Commentaries/calcom_sources"


## Leave these to build in subdir 'build'
BUILDDIR="`pwd`/build"
OSIS2MODOUTPUT="$BUILDDIR/modules/comments/zcom/calvincommentaries"
CONFDIR="$BUILDDIR/mods.d"
THISDIR=`pwd`


##################
COMBINED="calvincommentaries.versified.osis"


##############################################

function check_requirements {
	which csplit > /dev/null || { echo "Cannot find required tool 'csplit'.  Exiting."; exit 1;}
	which perl > /dev/null || { echo "Cannot find required tool 'perl'.  Exiting."; exit 1;}
	[ -d "$CALCOMSOURCES" ] || { echo "CALCOMSOURCES not set correctly -- please edit this file."; exit 1;}
	[ -d "$SWORDTOOLS" ] || { echo "SWORDTOOLS not set correctly -- please edit this file."; exit 1;}
}

function setup_dirs {
	mkdir -p $BUILDDIR
	mkdir -p $OSIS2MODOUTPUT
	mkdir -p $CONFDIR
}

function combine {
	echo "Running combine_calcom.py..."
	./combine_calcom.py "$CALCOMSOURCES"/calcom??.xml > "$BUILDDIR/calvincommentaries.thml" || exit 1
}

function convert_to_osis {
	echo "Converting to OSIS..."
	xsltproc --novalid  "$SWORDTOOLS/thml2osis/xslt/thml2osis.xslt" "$BUILDDIR/calvincommentaries.thml" > "$BUILDDIR/calvincommentaries.osis" || exit 1
}

function reversify {
	cd "$BUILDDIR"

	##############################################################################
	# Splitting
	# We currently have to use genbookOsis2Commentary (since
	# osis2mod doesn't accept format unless it is marked up like a Bible),
	# genbookOsis2Commentary is a quick hack, and doesn't work well
	# with big files, since it is DOM based.  So we split the file
	# into lots of small ones, using markers inserted before.
	# Then recombine again.  This is hacky, should go away once
	# osis2mod is fixed.

	# Split
	echo "Splitting..."

	rm part*

	COUNT=$(csplit -f 'part' -b '%03d' calvincommentaries.osis "/combine_calcom.py START/" '{*}' | nl | tail -n 1 | cut -c 1-7 )

	# $COUNT now contains the number of parts we split into

	FIRSTFILE="part000"
	FIRSTFILEALT="firstpart"
	LASTFILE="part`printf '%03d' $((COUNT-1))`"
	mv $FIRSTFILE $FIRSTFILEALT

	# $LASTFILE is special -- it will have trailing stuff
	TMP=`mktemp`
	perl -pne 's|</osis>||g; s|</osisText>||g' < $LASTFILE > $TMP || exit 1
	mv $TMP $LASTFILE


	# Fix individual files
	for F in part*;
	do
	    # prepend and append some stuff
	    TMP=`mktemp`
	    echo '<?xml version="1.0" encoding="UTF-8"?>' > $TMP
	    echo '<osis>' >> $TMP
	    echo '<osisText>' >> $TMP
	    cat $F >> $TMP
	    echo '</osisText>' >> $TMP
	    echo '</osis>' >> $TMP
	    mv $TMP $F

	    echo "re-versifying $F ..."
	    "$SWORDTOOLS/python/swordutils/osis/genbookOsis2Commentary.py" $F > "$F.versified" || exit 1

	    # Now strip stuff we added 
	    TMP2=`mktemp`
	    cat "$F.versified" | egrep -v 'xml version' | perl -pne 's|<osis>||g; s|<osisText>||g; s|</osis>||g; s|</osisText>||g;' > $TMP2
	    mv $TMP2 "$F.versified"

	done

	# Now combine again
	# Use this cleared up XML instead of the uncleaned stuff in $FIRSTFILEALT
	echo '<?xml version="1.0" encoding="UTF-8"?>' > $COMBINED
	echo '<osis xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.bibletechnologies.net/2003/OSIS/namespace http://www.bibletechnologies.net/osisCore.2.1.1.xsd">' >> $COMBINED
	echo '<osisText osisRefWork="bible" canonical="true" osisIDWork="calvincommentaries" xml:lang="en">' >> $COMBINED

	for F in part*.versified;
	do
	    cat $F >> $COMBINED
	done

	echo '</osisText>' >> $COMBINED
	echo '</osis>' >> $COMBINED

}

function convert_to_mod {

	cd "$BUILDDIR"

	# clean out old stuff
	rm -f "$OSIS2MODOUTPUT/*"

	# osis2mod
	echo "Running osis2mod..."
	osis2mod "$OSIS2MODOUTPUT" "$BUILDDIR/$COMBINED" -z -b 3 || exit 1
}

function do_zip {

    echo "Zipping..."
    cp "$THISDIR/calvincommentaries.conf" "$CONFDIR"

    cd "$BUILDDIR"
    rm -f CalvinCommentaries.zip
    zip -r CalvinCommentaries.zip mods.d/ modules/
}

function do_install {
    echo "Installing..."

    cd "$BUILDDIR"
    unzip -o -d $HOME/.sword CalvinCommentaries.zip

}

# The temporary files from each stage are left behind, so you
# just comment out the earlier stages if you only want
# to rerun the later stages.
check_requirements
setup_dirs
combine
convert_to_osis
reversify
convert_to_mod
do_zip
do_install
